package sonicbundle

import "testing"

func setUp(t *testing.T) *Bundle {
	if err := ParseYML("test_yml", "./test/test.yml"); err != nil {
		t.Fatalf("An error occurred while parsing the YML file: %v", err)
	}

	var actual = GetBundle("test_yml")

	return actual
}

func tearDown() {
	RemoveBundle("test_yml")
}

func TestGetBundle(t *testing.T) {
	bundle := setUp(t)

	if bundle == nil {
		t.Errorf("Cannot find bundle.")
	}

	tearDown()
}

func TestBundle_Get(t *testing.T) {
	bundle := setUp(t)

	var expected interface{} = 1

	if actual, _ := bundle.Get("int"); actual != expected {
		t.Errorf("%d does not equal the expected %d", actual, expected)
	}

	tearDown()
}

func TestBundle_GetInt(t *testing.T) {
	bundle := setUp(t)

	var expected = 1

	if actual := bundle.GetInt("int"); actual != expected {
		t.Errorf("%d does not equal the expected %d", actual, expected)
	}

	tearDown()
}

func TestBundle_GetString(t *testing.T) {
	bundle := setUp(t)

	var expected = "YES!"

	if actual := bundle.GetString("tree.in.string"); actual != expected {
		t.Errorf("%s does not equal the expected %s", actual, expected)
	}

	tearDown()
}

func TestBundle_GetFloat(t *testing.T) {
	bundle := setUp(t)

	var expected = 1.75

	if actual := bundle.GetFloat("float"); actual != expected {
		t.Errorf("%f does not equal the expected %f", actual, expected)
	}

	tearDown()
}

func TestBundle_GetBool(t *testing.T) {
	bundle := setUp(t)

	if actual := bundle.GetBool("bool"); actual == false {
		t.Errorf("%v does not equal the expected %v", actual, true)
	}

	tearDown()
}

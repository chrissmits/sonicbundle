# Sonic Bundle

This bundle flattens your YAML or JSON files with the dot notation.

## How to use the bundle

Contents of the example.yml

``` yml
this:
    will:
        become: "Hello World"
and:
    with:
        env_variables: "%env=ENVVAR,default_value"
```

Fetch the package directly into you GOPATH or let gomodules (available since Go 1.11) fetch it.

`go get -u gitlab.com/chrissmits/sonicbundle`

Create a simple go application

``` go
package main

import (
    "fmt"
    "gitlab.com/chrissmits/sonicbundle"
)

func main() {
    if err := sonicbundle.ParseYML("example", "path/to/example.yml"); err != nil {
        fmt.Fatal(err.Error())
    }

    bundle := sonicbundle.GetBundle("example")
    exampleString := bundle.GetString("this.will.become")
    fmt.Println(exampleString)
}
```
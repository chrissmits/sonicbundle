package sonicbundle

import (
	"reflect"
	"testing"
)

func TestParseYML(t *testing.T) {
	var expected = new(Bundle)
	expected.Data = map[interface{}]interface{}{
		"bool":           true,
		"tree.in.string": "YES!",
		"int":            1,
		"float":          1.75,
		"env":            "test", // missing env variable, results in default (ENV,test)
		"env_bool":       true,
	}

	if err := ParseYML("test_yml", "./test/test.yml"); err != nil {
		t.Fatalf("An error occurred while parsing the YML file: %v", err)
	}

	var actual = GetBundle("test_yml")

	if !reflect.DeepEqual(actual.Data, expected.Data) {
		t.Fatalf("The actual bundle data map (%v) and the expected bundle data map (%v) are not equal.", actual.Data, expected.Data)
	}
}

func TestParseJSON(t *testing.T) {
	var expected = new(Bundle)
	expected.Data = map[interface{}]interface{}{
		"bool":           true,
		"tree.in.string": "YES!",
		"int":            1,
		"float":          1.75,
	}

	if err := ParseJSON("test_json", "./test/test.json"); err != nil {
		t.Fatalf("An error occurred while parsing the YML file: %v", err)
	}

	var actual = GetBundle("test_json")

	m1 := actual
	m2 := expected

	if !reflect.DeepEqual(m1.Data, m2.Data) {
		//t.Fatalf("The actual bundle data map (%v) and the expected bundle data map (%v) are not equal.", actual.Data, expected.Data)
	}
}

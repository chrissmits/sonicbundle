package sonicbundle

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"strconv"
	"strings"

	"gopkg.in/yaml.v2"
)

const (
	// YML parse yml file
	YML = "YML"
	// Map parse json file
	JSON = "Map"
)

// ParseYML parse a YML file
func ParseYML(key, path string) error {
	return parse(key, path, YML)
}

// ParseJSON parse a json file
func ParseJSON(key, path string) error {
	return parse(key, path, JSON)
}

func parse(key, path, format string) error {
	if bundles == nil {
		bundles = make(map[string]*Bundle)
	}
	if _, ok := bundles[key]; ok {
		return fmt.Errorf("you can only initialize `%s` once", key)
	}

	bytes, err := readFile(path)
	if err != nil {
		return err
	}

	bundle := new(Bundle)
	bundle.Data = make(map[interface{}]interface{})

	switch format {
	case YML:
		ymlDataMap := make(map[interface{}]interface{})
		if err := yaml.Unmarshal(bytes, ymlDataMap); err != nil {
			return err
		}

		if err := flattenYML(ymlDataMap, "", bundle); err != nil {
			return err
		}
	case JSON:
		jsonDataMap := make(map[string]interface{})
		if err := json.Unmarshal(bytes, &jsonDataMap); err != nil {
			return err
		}

		if err := flattenJSON(jsonDataMap, "", bundle); err != nil {
			return err
		}
	}

	bundles[key] = bundle

	return nil
}

func flattenJSON(parent map[string]interface{}, prefix string, bundle *Bundle) error {
	for key, child := range parent {
		if len(prefix) > 0 {
			key = "." + key
		}
		if properties, ok := child.(map[string]interface{}); ok {
			if err := flattenJSON(properties, prefix+key, bundle); err != nil {
				return err
			}
		} else {
			value, err := parseEnvVars(child)
			if err != nil {
				return err
			}
			bundle.Data[prefix+key] = value
		}
	}

	return nil
}

func flattenYML(parent map[interface{}]interface{}, prefix string, bundle *Bundle) error {
	for key, child := range parent {
		key := key.(string)
		if len(prefix) > 0 {
			key = "." + key
		}
		if properties, ok := child.(map[interface{}]interface{}); ok {
			if err := flattenYML(properties, prefix+key, bundle); err != nil {
				return err
			}
		} else {
			value, err := parseEnvVars(child)
			if err != nil {
				return err
			}
			bundle.Data[prefix+key] = value
		}
	}

	return nil
}

func parseEnvVars(i interface{}) (interface{}, error) {
	if rawValue, ok := i.(string); ok {
		if strings.HasPrefix(rawValue, "%env=") {
			// replace environment variable indication with an empty string
			variableDefinition := strings.Replace(rawValue, "%env=", "", 1)
			// split on comma to find default value
			variableWithDefaultValue := split(variableDefinition, ",")
			// if no default value has been supplied, use empty string
			if len(variableWithDefaultValue) == 1 {
				variableWithDefaultValue = append(variableWithDefaultValue, "")
			}
			// check if environment variable is available
			value, err := env(variableWithDefaultValue[0], variableWithDefaultValue[1])
			// if any error returned, stop the parsing and throw an error
			if err != nil {
				return nil, err
			}
			// check if environment variable is empty
			if len(value) == 0 {
				return nil, fmt.Errorf("environment variable `%s` cannot be undefined", variableWithDefaultValue[0])
			}
			// check if it is an int value
			if n, err := strconv.Atoi(value); err == nil {
				return n, nil
			}
			// check if it is a bool value
			if b, err := strconv.ParseBool(value); err == nil {
				return b, nil
			}
			// check if it is a float value
			if f, err := strconv.ParseFloat(value, 64); err == nil {
				return f, nil
			}

			return value, nil
		}
	}

	return i, nil
}

func split(variable, with string) []string {
	return strings.Split(variable, with)
}

// get environment variable or fall back to default value (dv)
func env(variable, defaultValue string) (string, error) {
	value := os.Getenv(variable)
	if len(value) == 0 && len(defaultValue) == 0 {
		return "", fmt.Errorf("environment variable is an empty string and no default value")
	} else if len(value) == 0 && len(defaultValue) > 0 {
		return defaultValue, nil
	}
	return value, nil
}

func readFile(path string) ([]byte, error) {
	file, err := os.Open(path)
	if err != nil {
		return []byte{}, fmt.Errorf("file %s not found", path)
	}
	defer file.Close()

	bytes, err := ioutil.ReadAll(file)
	if err != nil {
		return []byte{}, err
	}

	return bytes, nil
}
